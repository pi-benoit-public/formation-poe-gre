"user strict";

$(document).ready(function() {

	// window.onload = localStorage.setItem("idUpdate",0);

	var jqxhr = $.ajax({
		method: "GET",
		url: "http://localhost:8080/api/users",
		crossDomain: true,
		dataType: 'jsonp'
	})
	.done(function() {})
	.then(function(data) {
		// console.log("success");
		var tr;
		if (data == "")
			$('tbody').append('<tr><td class="error" colspan="5">Pas d\'utilisateurs</td></tr>');
		$.each(data, function(i, item) {
			tr = $('<tr/>');
			tr.append("<td>" + item.firstName + "</td>");
			tr.append("<td>" + item.name + "</td>");
			tr.append("<td>" + item.email + "</td>");
			tr.append("<td>" + "********" + "</td>");
			// Note: button -> disable !!!
			tr.append('<td><span class="flex">' +
				'<button id="boutonUpdate" onclick="update('+ item.id +')" value="update" disable><img src="images/icon-edit.png"></button>' + '<button onclick="remove('+ item.id +')" value="remove" disable><img src="images/icon-delete.png"></button>' +
				'</span></td>');
			$('tbody').append(tr);
		});
	})
	// @todo
	// Le fail ne fonctionne pas quand dataType en jsonp est activé !!!!
	.fail(function() {
		// console.log( "error failed" );
		$('#list').append('<p class="error">Failed to load resource. Imposssible de se connecter au serveur</p>');
		$('tbody').append('<tr><td class="error" colspan="5">Pas d\'utilisateurs</td></tr>');
	})
	.always(function() {
		// console.log( "complete" );
	});

	function update(id) {
		alert("bientôt");
	}

	function remove(id) {
		alert("bientôt");
	}

});
