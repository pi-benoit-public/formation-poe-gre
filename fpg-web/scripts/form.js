"use strict";

$(document).ready(function() {
	addUser();
});

function addUser() {

	// process the form
	$('#formUserAdd').submit(function(e) {

		e.preventDefault();

		// get the form data
		var formData = {
			'id'				: $('input#id').val(),
			'name'			: $('input#name').val(),
			'firstName'	: $('input#firstName').val(),
			'email'			: $('input#email').val(),
			'password'	: $('input#password').val(),
			'zipCode'		: $('input#zipCode').val()
		};

		// process the form
		$.ajax({
			type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
			url         : 'http://localhost:8080/api/users/add', // the url where we want to POST
			data        : formData, // our data object
			dataType    : 'jsonp', // what type of data do we expect back from the server
			encode      : true,
			crossDomain : true
		})
		// using the done promise callback
		.done(function(data) {

			// log data to the console so we can see
			console.log(data);

			// here we will handle errors and validation messages
			// @todo: A déplacer.
			// Ne pas le laisser là, car le formulaire est envoyé quand même.
			if (data.password != $('input#password2'))
				$('<p class="error">Le mot de passe ne correspond pas.</p>').insertAfter('input#password2');

			// Redirection si ok.
			window.location.href = "index.html";

		});

	});

}
