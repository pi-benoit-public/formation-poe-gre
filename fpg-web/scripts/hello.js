$(document).ready(function() {
	$.ajax({
		url : "http://localhost:8080/api/hello/biloute",
		// used to hack access-allow-same-origin errors
		// in a wonderful world, the webapp will use a proxy to be on the
		// same host:port :-)
		crossDomain : true,
		dataType : 'jsonp'
	}).then(function(data) {
		$('.greeting-content').append(data.name);
	});
});
