# formation POE: Exercice

Les deux projets contenus dans l'archive sont les projets sur lesquels vous devrez intervenir pour l'exercice.
Le but de cet exercice est de produire une petite application qui permet de lister les utilisateurs et d'en 
ajouter, les éditer...

Les projets sont déjà exécutables et fonctionnent ensemble :-) 

Le projet _fpg-web_ contient l'application web et le projet _fpg-api_ contient l'API Rest.

## Scénario

Le scénario de l'exercice est le suivant:

Quand j'accède à l'application web, nous affichons la liste des utilisateurs. Cette liste doit correspondre à la
maquette fournit nommée : 
[02-User-list-01.png]()


Ensuite, lorsque l'on clique sur _ajouter un utilisateur_, vous devez naviguer vers une nouvelle page et créer un 
formulaire pour permettre de créer un utilisateur et de le stocker en base de données. Il faudra bien entendu 
envoyer les données au serveur backend :-)

Les contrôles à faire sur la validation du formulaire sont:

* le nom de l'utilisateur est un champ obligatoire
* l'email doit correspondre à une adresse email valide
* les deux mots de passe de l'utilisateur doivent être identiques et contenir plus de 6 caractères

Le formulaire et les messages d'erreurs doivent correspondre à la maquette fournit nommée : 
[01-Form-new-user-02-error.png]()

Lorsque tous les champs du formulaire sont corrects et que je valide le formulaire, l'utilisateur est enregistré 
en base de données et nous affichons la liste des utilisateurs enregistrés.

## Ressources

Les icones sont disponibles dans le répertoire _images_ à la racine du projet. La typo a utilisé est: 

* roboto (google fonts).

Et les couleurs sont les suivantes:

* Couleur bleu : #4D87E3
* Couleur rouge : #F75252

### Les URLS de l'API Rest

#### Créer un utilisateur

* POST /api/users 
* responseCode: 201

Avec un utilisateur dans le corps de la requete (@RequestBody)

Voici un exemple de code pour un méthode de création d'un utilisateur:

```java
    /**
     * POST  /users -> Creates a new user.
     * <p>
     * Creates a new user if the login and email are not already used, and sends an
     * mail with an activation link.
     * The user needs to be activated on creation.
     * </p>
     */
    @RequestMapping(value = "/users",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> createUser(@RequestBody ManagedUserDTO managedUserDTO, HttpServletRequest request) throws URISyntaxException {
        log.debug("REST request to save User : {}", managedUserDTO);
        if (userRepository.findOneByLogin(managedUserDTO.getLogin()).isPresent()) {
            return ResponseEntity.badRequest()
                .body("Login already in use");
        } else if (userRepository.findOneByEmail(managedUserDTO.getEmail()).isPresent()) {
            return ResponseEntity.badRequest()
                .body("Email already in use");
        } else {
            User newUser = userService.createUser(managedUserDTO);
            String baseUrl = request.getScheme() + // "http"
            "://" +                                // "://"
            request.getServerName() +              // "myhost"
            ":" +                                  // ":"
            request.getServerPort() +              // "80"
            request.getContextPath();              // "/myContextPath" or "" if deployed in root context
            mailService.sendCreationEmail(newUser, baseUrl);
            return ResponseEntity.created(new URI("/api/users/" + newUser.getLogin()))
                .body(newUser);
        }
    }
```

#### Lister les utilisateurs 

* GET /api/users 
* responseCode: 200

Avec la liste des utilisateurs dans le corps de la réponse

Si il n'y a pas d'utilisateur, renvoyer un code 200 avec une liste d'utilisateurs vide

#### Récupérer un utilisateur 

* GET /api/users/{userId} 
* responseCode: 200

userId: correspond à l'identifiant de l'utilisateur

Avec l'utilisateur dans le corps de la réponse

Si il n'y a pas d'utilisateur associé à l'identifiant, renvoyer un code 404 (NotFound)

#### Modifier un utilisateur

* PUT /api/users/{userId}  
* responseCode: 200

userId: correspond à l'identifiant de l'utilisateur

Avec un utilisateur dans le corps de la requete (@RequestBody) et l'utilisateur modifié dans le corps de la réponse

#### Supprimer un utilisateur

* DELETE /api/users/{userId}  
* responseCode: 200

userId: correspond à l'identifiant de l'utilisateur


Voici des urls de ressources pour les API Rest

* [mooc api Rest](https://openclassrooms.com/courses/utilisez-des-api-rest-dans-vos-projets-web)
* [Tuto](https://la-cascade.io/concevoir-une-api/)


### Utiliser Spring Boot

Voici des urls de ressources pour Spring Boot

* [getting started](https://spring.io/guides/gs/spring-boot/)
* [7 things](https://dzone.com/articles/7-things-to-know-getting-started-with-spring-boot)
