# formation POE

## Introduction

Le projet contient deux projets:

* fpg-web: projet web qui contient l'interface web du projet
* fpg-api: projet Java qui contient l'API Rest qui sera utilisée par le projet web


## Projets

### fpg-web

#### Contenu

Ce projet contient l'interface web du projet. C'est un projet XXX


#### Lancer le projet


### fpg-api

#### Contenu

Ce projet contient l'API Rest utilisée par la partie web. 
Il est basé sur Spring Boot et utilise Maven comme outil de build. Il est construit en tant que _jar_
executable et donc nous n'aurons pas besoin de serveur d'application pour le lancer.

De base, l'arborescence du projet est la suivante:

```
src/
├── main
│   ├── java
│   │   └── fr
│   │       └── hardis
│   │           └── formation
│   │               └── poe
│   │                   └── fpgapi
│   │                       ├── FpgApiApplication.java
│   │                       └── api
│   │                           └── FpgApiUserResource.java
│   └── resources
│       └── application.properties
└── test
```

Le projet contient la classe de lancement et une ressource Rest exemple.

#### Lancer le projet

Toutes les commandes citées ci-dessous sont à lancer avec un terminal Windows ou Linux.

Pour compiler et construire le projet, lancer la commande suivante:

    ./mvnw package

Pour lancer le projet, lancer la commande suivante:

    java -jar target/fpg-api-0.0.1-SNAPSHOT.jar

Ou, plus simplement, lancer la commande suivante pour compiler et lancer l'api avec le plugin
Spring Boot pour Maven.

    ./mvnw spring-boot:run
 
Votre application est disponible sur l'url suivante: 

    http://localhost:8080


## Consignes

Les deux projets sont séparés donc il faut lancer les deux projets pour les faire fonctionner ensemble. Donc, 
il faut lancer le projet _fpg-api_ avant de lancer le projet _fpg-web_ pour que le web puisse se connecter
à l'API.

Utilisation de Spring Data JPA pour l'accès aux données

* https://spring.io/guides/gs/accessing-data-jpa/

### Prérequis

Les prérequis sont les suivants pour pouvoir commencer à développer: 

* Un jdk Java 1.8 minimum (pas de jre)
* Maven comme outil de build pour construire l'API Rest

L'exercice est décrit dans le fichier TODO.md :-)
