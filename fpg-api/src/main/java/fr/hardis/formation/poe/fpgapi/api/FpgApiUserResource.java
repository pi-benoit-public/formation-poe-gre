package fr.hardis.formation.poe.fpgapi.api;

import fr.hardis.formation.poe.fpgapi.model.User;
import fr.hardis.formation.poe.fpgapi.service.UserService;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class FpgApiUserResource {

	private final Logger log = LoggerFactory.getLogger(FpgApiUserResource.class);

	@Autowired
	UserService us;

	/**
	 * Hello world resource
	 */
	@RequestMapping(value = "/hello/{login}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<User> deleteUser(@PathVariable String login) {
		log.debug("REST request to hello : {}", login);
		User user = new User();
		user.setName(login);
		return ResponseEntity.ok().body(user);
	}

	@RequestMapping(value = "/users", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<User>> getListUsers() {
		List<User> users = us.getUsers();
		log.info("REST request to getListUsers : {}", users);
		return ResponseEntity.ok().body(users);
	}

	/*
	 * Add user
	 */
	@RequestMapping(value = "/users/add", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<User> addUser(@RequestParam Long id, @RequestParam String name, @RequestParam String firstName,
			@RequestParam String email, @RequestParam String password, @RequestParam String zipCode) {
		User user = new User();
		user.setName(name);
		user.setFirstName(firstName);
		user.setEmail(email);
		user.setPassword(password);
		user.setZipCode(zipCode);
		log.info("REST request to user created: {}", user.toString());
		us.saveUser(user);
		return new ResponseEntity<User>(user, HttpStatus.OK);
	}

}
