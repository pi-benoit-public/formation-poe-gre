package fr.hardis.formation.poe.fpgapi.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.hardis.formation.poe.fpgapi.dao.UserDAO;
import fr.hardis.formation.poe.fpgapi.model.User;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserDAO ud;

	public void saveUser(User user) {
		ud.save(user);
	}

	public User getUser(long id) {
		return ud.getOne(id);
	}

	public List<User> getUsers() {
		return ud.findAll();
	}

	public void deleteUser(User user) {
		ud.delete(user);
	}

}
