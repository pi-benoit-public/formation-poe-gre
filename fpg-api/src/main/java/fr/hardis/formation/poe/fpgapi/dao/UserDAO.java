package fr.hardis.formation.poe.fpgapi.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.hardis.formation.poe.fpgapi.model.User;

public interface UserDAO extends JpaRepository<User, Long> {

}
