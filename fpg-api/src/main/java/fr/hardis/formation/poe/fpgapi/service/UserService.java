package fr.hardis.formation.poe.fpgapi.service;

import java.util.List;

import fr.hardis.formation.poe.fpgapi.model.User;

public interface UserService {

	public void saveUser(User user);

	public User getUser(long id);

	public List<User> getUsers();

	public void deleteUser(User user);

}
